# Routes Explaination :

## Artist :

### - /api/artists/ GET route

**Response:**<br/>
status code: `200`<br/>
body: JSON, the object should contain three fields:<br/>
`message` - "artists were fetched"<br/>
`status` - 200<br/>
`result` - artists objects<br/>

### - /api/artists/ POST route

**Body parameters:**<br/>
`firstName `- string, first name of user (e.g: "Or"), **required**<br/>
`lastName `- string, last name of user (e.g: "Schweitzer"), **required**<br/>
`age` - number, birth year of user (e.g: 26), **required**<br/>

**Response:**<br/>
If inserted valid input:<br/>
status code: `200`<br/>
body: JSON, the object should contain three fields:<br/>
`message` - "artist was created succesfuly"<br/>
`status` - 200<br/>
`result` - created artist object<br/>

If the request doesn’t contain valid input, the response will contain a proper status code + json object with details of the error.

### - /api/artists/ PUT route

**Body parameters:**<br/>
`firstName `- string, first name of user (e.g: "Or")<br/>
`lastName `- string, last name of user (e.g: "Schweitzer")<br/>
`age` - number, birth year of user (e.g: 26)<br/>

**Response:**<br/>
If inserted valid input:<br/>
status code: `200`<br/>
body: JSON, the object should contain three fields:<br/>
`message` -"User has been updated succesfuly"<br/>
`status` - 200<br/>
`result` - updated artist object<br/>

If the request doesn’t contain valid input, the response will contain a proper status code + json object with details of the error.

### - /api/artists/:id GET route

**Query parameters:**<br/>
`id `- string, id of artist<br/>

**Response:**<br/>
If inserted valid input:<br/>
status code: `200`<br/>
body: JSON, the object should contain three fields:<br/>
`message` - "artist fetched succesfuly"<br/>
`status` - 200<br/>
`result` - artist object<br/>

If the request doesn’t contain valid input, the response will contain a proper status code + json object with details of the error.

### - /api/artists/:id DELETE route

**Query parameters:**<br/>
`id `- string, id of artist<br/>

**Response:**<br/>
If inserted valid input:<br/>
status code: `200`<br/>
body: JSON, the object should contain three fields:<br/>
`message` - "artist deleted succesfuly"<br/>
`status` - 200<br/>
`result` - deleted artist object<br/>

If the request doesn’t contain valid input, the response will contain a proper status code + json object with details of the error.
