import { HttpError } from "./http.exception.js";

export class UnauthorizedError extends HttpError {
    constructor() {
        super("Unauthorized Error.", 401);
    }
}
