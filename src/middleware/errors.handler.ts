import log from "@ajar/marker";
import fs from "fs";
import { Request, Response, NextFunction } from "express";
import { HttpError } from "../exceptions/http.exception.js";
import { ResponseMessage } from "../types/response.types.js";
const { White, Reset, Red } = log.constants;

export const printError = (
    err: HttpError,
    req: Request,
    res: Response,
    next: NextFunction
) => {
    log.error(err);
    next(err);
};

export const errorLogger = (filePath: string) => {
    const writeStream = fs.createWriteStream(filePath, {
        encoding: "utf-8",
        flags: "a",
    });

    return function (
        err: HttpError,
        req: Request,
        res: Response,
        next: NextFunction
    ) {
        const { message, stack, status } = err;
        writeStream.write(
            `${req.userId} - ${status} :: ${message} >> ${stack}\n`
        );
        next(err);
    };
};

export const errorResponse = (
    err: HttpError,
    req: Request,
    res: Response,
    next: NextFunction
) => {
    const response: ResponseMessage = {
        status: err.status,
        message: err.message,
        data: err.stack,
    };

    res.status(err.status).json(response);
};

export const not_found = (req: Request, res: Response) => {
    log.info(`url: ${White}${req.url}${Reset}${Red} not found...`);
    res.status(404).json({ status: `url: ${req.url} not found...` });
};
