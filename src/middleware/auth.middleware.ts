import { RequestHandler } from "express";
import jwt from "jsonwebtoken";
import { ILoginDetails, JwtPayload } from "../modules/auth/auth.types.js";
import { UnauthorizedError } from "../exceptions/unauthorized.exception.js";

const { APP_SECRET } = process.env;

export const verifyAuth: RequestHandler = async (req, res, next) => {
    const accessToken = req.headers["x-access-token"] as string | undefined;
    if (!accessToken) throw new UnauthorizedError();
    try {
        const { userId } = (await jwt.verify(
            accessToken,
            APP_SECRET as string
        )) as JwtPayload;

        req.userId = userId;

        next();
    } catch (error) {
        throw new UnauthorizedError();
    }
};
