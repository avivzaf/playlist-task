import express, { Express } from "express";
import morgan from "morgan";
import log from "@ajar/marker";
import cors from "cors";
import { connect_db } from "./db/mongoose.connection.js";
import user_router from "./modules/user/user.router.js";
import artist_router from "./modules/artist/artist.router.js";
import song_router from "./modules/song/song.router.js";
import authRouter from "./modules/auth/auth.router.js"
import {
    printError,
    errorResponse,
    not_found,
    errorLogger,
} from "./middleware/errors.handler.js";
import { logger } from "./middleware/logger.handler.js";
import cookieParser from 'cookie-parser';
class App {
    private readonly PORT: number;
    private readonly HOST: string;
    private readonly DB_URI: string;
    private readonly app: Express;

    constructor() {
        const { PORT = 8080, HOST = "localhost", DB_URI } = process.env;
        this.PORT = Number(PORT);
        this.HOST = String(HOST);
        this.DB_URI = String(DB_URI);
        this.app = express();
    }

    initMiddlewares() {
        this.app.use(cors());
        this.app.use(morgan("dev"));
        this.app.use(express.json());
        this.app.use(cookieParser())
        this.app.use(logger("./loggers/requestLogger"));
    }

    initRoutingFunctions() {
        this.app.use("/api/users", user_router);
        this.app.use("/api/artists", artist_router);
        this.app.use("/api/songs", song_router);
        this.app.use("/api/auth", authRouter.router);
    }

    handleErrors() {
        this.app.use(printError);
        this.app.use(errorLogger("./loggers/errorLogger"));
        this.app.use(errorResponse);
    }

    handleNoMatchingRoutes() {
        this.app.use("*", not_found);
    }

    async connectServer() {
        try {
            await connect_db(this.DB_URI);
            await this.app.listen(Number(this.PORT), this.HOST as string, () =>
                log.magenta(
                    `api is live on`,
                    ` ✨ ⚡  http://${this.HOST}:${this.PORT} ✨ ⚡`
                )
            );
        } catch (err) {
            console.log(err);
        }
    }

    async startServer() {
        this.initMiddlewares();
        this.initRoutingFunctions();
        this.handleErrors();
        this.handleNoMatchingRoutes();
        await this.connectServer();
    }
}

const app = new App();
app.startServer();
