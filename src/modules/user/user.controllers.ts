import { userService } from "./user.services.js";
import { InstanceNotFoundError } from "../../exceptions/instanceNotFound.exception.js";
import { Request, Response } from "express";
import { ResponseMessage } from "../../types/response.types.js";

class UserController {
    getAllUser = async (req: Request, res: Response) => {
        const users = await userService.getAllUsers();
        const reposonse: ResponseMessage = {
            message: "Users fetched succesfuly",
            status: 200,
            data: users,
        };

        res.status(200).json(reposonse);
    };

    getSingleUser = async (req: Request, res: Response) => {
        try {
            const user = await userService.getSingleUser(req.params.id);
            const reposonse: ResponseMessage = {
                message: "User fetched succesfuly",
                status: 200,
                data: user,
            };

            res.status(200).json(reposonse);
        } catch (err) {
            throw new InstanceNotFoundError("User not found", 404);
        }
    };

    updateUser = async (req: Request, res: Response) => {
        const user = await userService.updateUser(req.params.id, req.body);
        const reposonse: ResponseMessage = {
            message: "User has been updated succesfuly",
            status: 200,
            data: user,
        };

        res.status(200).json(reposonse);
    };

    deleteUser = async (req: Request, res: Response) => {
        try {
            const user = await userService.deleteUser(req.params.id);
            const reposonse: ResponseMessage = {
                message: "User has been deleted succesfuly",
                status: 200,
                data: user,
            };

            res.status(200).json(reposonse);
        } catch (err) {
            throw new InstanceNotFoundError("User not found", 404);
        }
    };
}

export const userController = new UserController();
