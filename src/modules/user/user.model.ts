import mongoose from "mongoose";
const { Schema, model } = mongoose;

const UserSchema = new Schema({
    first_name: {
        type: String,
        minLength: 3,
        maxLength: 30,
    },
    last_name: {
        type: String,
        minLength: 3,
        maxLength: 30,
    },
    email: { type: String, required: [true, "Email is required"] },
    password: {
        type: String,
        required: [true, "Password is required"],
    },
    refreshToken: {
        type: String,
    },
    phone: {
        type: String,
        minLength: 10,
        maxLength: 10,
    },
});

export default model("user", UserSchema);
