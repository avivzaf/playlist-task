import user_model from "./user.model.js";
import { IUpdateUser, ICreateUser } from "./user.types.js";

class UserDB {
    getAllUsers = async () =>
        await user_model.find().select(`-_id 
        first_name 
        last_name 
        email 
        phone`);

    createNewUser = async (userDetails: ICreateUser) =>
        await user_model.create(userDetails);

    getSingleUser = async (id: string) => await user_model.findById(id);

    getUserByEmail = async (email: string) =>
        await user_model.findOne({ email });

    updateUser = async (id: string, userDetails: IUpdateUser) =>
        await user_model.findByIdAndUpdate(id, userDetails, {
            new: true,
            upsert: false,
        });

    deleteUser = async (id: string) => await user_model.findByIdAndRemove(id);
}

export const userDB = new UserDB();
