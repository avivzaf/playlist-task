import Joi from "joi";
import { Request, Response, NextFunction } from "express";
import { ICreateUser, IUpdateUser, UserValidation } from "./user.types.js";
import { InvalidArgumentsError } from "../../exceptions/invalidArguments.exception.js";

const validateUser = async (
    user: ICreateUser | IUpdateUser,
    type: UserValidation = UserValidation.UPDATE
) => {
    let first_name = Joi.string().alphanum().min(3).max(30);
    let last_name = Joi.string().alphanum().min(3).max(30);
    let password = Joi.string();
    let email = Joi.string().email({
        minDomainSegments: 2,
        tlds: { allow: ["com", "net"] },
    });
    let phone = Joi.string().pattern(new RegExp("^[0-9]+$")).length(10);

    if (type == UserValidation.CREATE) {
        email = email.required();
        password = password.required();
    }
    const schema = Joi.object().keys({
        first_name,
        last_name,
        password,
        email,
        phone,
    });

    try {
        return await schema.validateAsync(user);
    } catch (err: any) {
        throw new InvalidArgumentsError(err.details[0].message, 404);
    }
};

export const validationFactoryMethod = (type: UserValidation) => {
    return async (req: Request, res: Response, next: NextFunction) => {
        await validateUser(req.body, type);
        next();
    };
};
