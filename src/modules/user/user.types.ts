export interface IUser {
    first_name?: string;
    last_name?: string;
    phone?: string;
    email: string;
    password: string;
    refreshToken: string | null;
}

export type ICreateUser = Omit<IUser, 'refreshToken'>;

export type IUpdateUser = Partial<IUser>;

export enum UserValidation {
    CREATE,
    UPDATE,
}