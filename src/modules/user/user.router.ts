import raw from "../../utils/route.async.wrapper.js";
import express from "express";
import { validationFactoryMethod } from "./user.validation.js";
import { userController } from "./user.controllers.js";
import { UserValidation } from "./user.types.js";

const router = express.Router();

router.get("/", raw(userController.getAllUser));
router.get("/:id", raw(userController.getSingleUser));


// these 2 routes not supposed to be here
router.put(
    "/:id",
    raw(validationFactoryMethod(UserValidation.UPDATE)),
    raw(userController.updateUser)
);
router.delete("/:id", raw(userController.deleteUser));

export default router;
