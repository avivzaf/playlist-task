import { userDB } from "./user.db.js";
import { IUpdateUser, ICreateUser } from "./user.types.js";

class UserService {
    createNewUser = async (userDetails: ICreateUser) =>
        await userDB.createNewUser(userDetails);

    getAllUsers = async () => await userDB.getAllUsers();

    getSingleUser = async (id: string) => await userDB.getSingleUser(id); //change by id

    getUserByEmail = async (email: string) =>
        await userDB.getUserByEmail(email);

    updateUser = async (id: string, userDetails: IUpdateUser) =>
        await userDB.updateUser(id, userDetails);

    deleteUser = async (id: string) => await userDB.deleteUser(id);
}

export const userService = new UserService();
