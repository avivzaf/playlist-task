import mongoose from "mongoose";
const { Schema, model } = mongoose;

const ArtistSchema = new Schema({
    first_name: {
        type: String,
        required: [true, "First name is required"],
        minLength: 2,
        maxLength: 30,
    },
    last_name: {
        type: String,
        required: [true, "Last name is required"],
        minLength: 2,
        maxLength: 30,
    },
    age: {
        type: Number,
        required: [true, "Age is required"],
        minLength: 0,
        maxLength: 100,
    },
    songs: [{ type: Schema.Types.ObjectId, ref: "song" }],
});

export default model("artist", ArtistSchema);
