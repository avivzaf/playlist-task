import Joi from "joi";
import { Request, Response, NextFunction } from "express";
import { IArtist, IUpdateArtist, ArtistValidation } from "./artist.types.js";
import { InvalidArgumentsError } from "../../exceptions/invalidArguments.exception.js";

const validateArtist = async (artist: IArtist | IUpdateArtist, type:ArtistValidation = ArtistValidation.UPDATE) => {
    let first_name = Joi.string().alphanum().min(2).max(30);
    let last_name = Joi.string().alphanum().min(2).max(30);
    let age = Joi.number().min(0).max(100);

    if (type == ArtistValidation.CREATE) {
        first_name = first_name.required();
        last_name = last_name.required();
        age = age.required();
    }

    const schema = Joi.object().keys({ first_name, last_name, age });

    try {
        return await schema.validateAsync(artist);
    } catch (err: any) {
        throw new InvalidArgumentsError(err.details[0].message, 404);
    }
};

export const validationFactoryMethod = (type: ArtistValidation) => {
    return async (req: Request, res: Response, next: NextFunction) => {
        await validateArtist(req.body, type);
        next();
    };
};