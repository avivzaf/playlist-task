import raw from "../../utils/route.async.wrapper.js";
import express from "express";
import { validationFactoryMethod } from "./artist.validation.js";
import { artistController } from "./artist.controllers.js";
import { logger } from "../../middleware/logger.handler.js";
import { ArtistValidation } from "./artist.types.js"

const router = express.Router();



router.post(
    "/",
    raw(validationFactoryMethod(ArtistValidation.CREATE)),
    raw(artistController.createNewArtist)
);
router.get("/", raw(artistController.getAllArtist));
router.get("/:id", raw(artistController.getSingleArtist));
router.put(
    "/:id",
    raw(validationFactoryMethod(ArtistValidation.UPDATE)),
    raw(artistController.updateArtist)
);
router.delete("/:id", raw(artistController.deleteArtist));

export default router;
