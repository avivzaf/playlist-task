import artist_model from "./artist.model.js";
import { IUpdateArtist, IArtist} from "./artist.types.js";

class ArtistDB {
    getAllArtists = async () =>
        await artist_model.find().select(`-_id 
        first_name 
        last_name 
        age 
        songs`);

    createNewArtist = async (artistDetails: IArtist) =>
        await artist_model.create(artistDetails);

    getSingleArtist = async (id: string) => await artist_model.findById(id);

    updateArtist = async (id: string, artistDetails: IUpdateArtist) =>
        await artist_model.findByIdAndUpdate(id, artistDetails, {
            new: true,
            upsert: false,
        });

    deleteArtist = async (id: string) => await artist_model.findByIdAndRemove(id);
}

export const artistDB = new ArtistDB();
