export interface IArtist {
    first_name: string;
    last_name: string;
    age: number;
}

export type IUpdateArtist = Partial<IArtist>;

export enum ArtistValidation {
    CREATE,
    UPDATE,
}
