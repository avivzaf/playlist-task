import { artistDB } from "./artist.db.js";
import { IUpdateArtist, IArtist } from "./artist.types.js";
import { songService } from "../song/song.services.js";
import pkg from "bluebird";

const { Promise } = pkg;

class ArtistService {
    createNewArtist = async (artistDetails: IArtist) =>
        await artistDB.createNewArtist(artistDetails);

    getAllArtists = async () => await artistDB.getAllArtists();

    getSingleArtist = async (id: string) => await artistDB.getSingleArtist(id);

    updateArtist = async (id: string, artistDetails: IUpdateArtist) =>
        await artistDB.updateArtist(id, artistDetails);

    deleteArtist = async (id: string) => {
        const artistToDelete = await artistDB.getSingleArtist(id);
        const songsToDelete = [...artistToDelete.songs];

        await Promise.each(songsToDelete, (songID: any) =>
            songService.deleteSong(songID)
        );

        return await artistDB.deleteArtist(id);
    };

    removeSongFromArtist = async (songIdToRemove: string) => {
        const song = await songService.getSingleSong(songIdToRemove);
        const artistToUpdate = await artistService.getSingleArtist(song.artist);
        artistToUpdate.songs = artistToUpdate.songs.filter(
            (songID: any) => songID.toString() !== song._id.toString()
        );

        await artistToUpdate.save();
    };

    addSongToArtist = async (songID: string, artistID: string) => {
        const artist = await artistDB.getSingleArtist(artistID);
        artist.songs.push(songID);
        await artist.save();
    };
}

export const artistService = new ArtistService();
