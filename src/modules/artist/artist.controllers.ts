import { artistService } from "./artist.services.js";
import { InstanceNotFoundError } from "../../exceptions/instanceNotFound.exception.js";
import { Request, Response } from "express";
import { ResponseMessage } from "../../types/response.types.js";

class ArtistController {
    createNewArtist = async (req: Request, res: Response) => {
        const artist = await artistService.createNewArtist(req.body);
        const reposonse: ResponseMessage = {
            message: "Artist created succesfuly",
            status: 200,
            data: artist,
        };

        res.status(200).json(reposonse);
    };

    getAllArtist = async (req: Request, res: Response) => {
        const artists = await artistService.getAllArtists();
        const reposonse: ResponseMessage = {
            message: "Artists fetched succesfuly",
            status: 200,
            data: artists,
        };

        res.status(200).json(reposonse);
    };

    getSingleArtist = async (req: Request, res: Response) => {
        try {
            const artist = await artistService.getSingleArtist(req.params.id);
            const reposonse: ResponseMessage = {
                message: "Artist fetched succesfuly",
                status: 200,
                data: artist,
            };

            res.status(200).json(reposonse);
        } catch (err) {
            throw new InstanceNotFoundError("Artist not found", 404);
        }
    };

    updateArtist = async (req: Request, res: Response) => {
        const artist = await artistService.updateArtist(
            req.params.id,
            req.body
        );
        const reposonse: ResponseMessage = {
            message: "Artist has been updated succesfuly",
            status: 200,
            data: artist,
        };

        res.status(200).json(reposonse);
    };

    deleteArtist = async (req: Request, res: Response) => {
        try {
            const artist = await artistService.deleteArtist(req.params.id);
            const reposonse: ResponseMessage = {
                message: "Artist has been deleted succesfuly",
                status: 200,
                data: artist,
            };

            res.status(200).json(reposonse);
        } catch (err) {
            throw new InstanceNotFoundError("Artist not found", 404);
        }
    };
}

export const artistController = new ArtistController();
