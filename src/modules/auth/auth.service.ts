/* eslint-disable @typescript-eslint/no-explicit-any */
import jwt from "jsonwebtoken";
import bcrypt from "bcryptjs";
import { ILoginDetails, JwtPayload } from "./auth.types.js";
import { userService } from "../user/user.services.js";
import { ICreateUser, IUser } from "../user/user.types.js";

const { APP_SECRET, ACCESS_TOKEN_EXPIRATION, REFRESH_TOKEN_EXPIRATION } =
    process.env;

class AuthService {
    private generateAccessToken(payload: any) {
        return jwt.sign(payload, APP_SECRET as string, {
            expiresIn: ACCESS_TOKEN_EXPIRATION,
        });
    }

    private generateRefreshToken(payload: any) {
        return jwt.sign(payload, APP_SECRET as string, {
            expiresIn: REFRESH_TOKEN_EXPIRATION,
        });
    }

    private generateTokens(payload: any) {
        return {
            accessToken: this.generateAccessToken(payload),
            refreshToken: this.generateRefreshToken(payload),
        };
    }

    async signup(userDetails: ICreateUser) {
        const salt = await bcrypt.genSalt();

        userDetails.password = await bcrypt.hash(userDetails.password, salt);

        const user = await userService.createNewUser(userDetails);
        const tokens = this.generateTokens({ userId: user._id });

        await userService.updateUser(user._id, {
            refreshToken: tokens.refreshToken,
        });

        return tokens;
    }

    async login(credentials: ILoginDetails) {
        const { email, password } = credentials;

        const user = await userService.getUserByEmail(email);
        //if (!user) return false;

        const isEqual = await bcrypt.compare(password, user.password);
        //if (!isEqual) return false;

        const tokenPayload = { userId: user._id };
        const tokens = this.generateTokens(tokenPayload);

        await userService.updateUser(user._id, {
            refreshToken: tokens.refreshToken,
        });

        return tokens;
    }

    async logout(userId: string) {
        const user = await userService.updateUser(userId, {
            refreshToken: null,
        });
        return user;
    }

    async getAccessToken(refreshToken: string) {
        if (!refreshToken) return null;
        let decoded;
        try {
            decoded = (await jwt.verify(
                refreshToken,
                APP_SECRET as string
            )) as JwtPayload;
        } catch (err) {
            return null;
        }

        const { userId } = decoded;
        const user = await userService.getSingleUser(userId);
        //if (!user) return null;

        if (user.refreshToken !== refreshToken) return null;

        const accessToken = this.generateAccessToken({ userId });

        return accessToken;
    }
}

const authService = new AuthService();

export default authService;
