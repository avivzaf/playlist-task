import { RequestHandler } from "express";
import ms from "ms";
import { UnauthorizedError } from "../../exceptions/unauthorized.exception.js";
import { ResponseMessage } from "../../types/response.types.js";
import { ILoginDetails } from "./auth.types.js";
import authService from "./auth.service.js";
import { ICreateUser } from "../user/user.types.js";

const { REFRESH_TOKEN_EXPIRATION } = process.env;

class AuthController {
    login: RequestHandler = async (req, res) => {
        const loginCredentials: ILoginDetails = req.body;
        const tokens = await authService.login(loginCredentials);
        //if (!tokens) throw new UnauthorizedError();
        const { accessToken, refreshToken } = tokens;

        const response: ResponseMessage = {
            status: 200,
            message: "success",
            data: { accessToken },
        };

        res.cookie("refreshToken", refreshToken, {
            maxAge: ms(REFRESH_TOKEN_EXPIRATION as string),
            httpOnly: true,
        })
            .json(response)
            .status(response.status);
    };

    logout: RequestHandler = async (req, res) => {
        const user = await authService.logout(req.userId);

        const response: ResponseMessage = {
            status: 204,
            message: "success",
            data: { user },
        };

        res.clearCookie("refreshToken").json(response).status(response.status);
    };

    signup: RequestHandler = async (req, res) => {
        const tokens = await authService.signup(req.body);
        const { accessToken, refreshToken } = tokens;

        const response: ResponseMessage = {
            status: 201,
            message: "User created succesfuly",
            data: { accessToken },
        };

        res.cookie("refreshToken", refreshToken, {
            maxAge: ms(REFRESH_TOKEN_EXPIRATION as string),
            httpOnly: true,
        })
            .status(response.status)
            .json(response);
    };

    getAccessToken: RequestHandler = async (req, res) => {
        const { refreshToken } = req.cookies;
        const accessToken = await authService.getAccessToken(refreshToken);
        if (!accessToken) throw new UnauthorizedError();

        const response: ResponseMessage = {
            status: 200,
            message: "success",
            data: { accessToken },
        };

        res.status(response.status).json(response);
    };
}

const authController = new AuthController();

export default authController;
