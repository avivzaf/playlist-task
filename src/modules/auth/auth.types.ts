export interface ILoginDetails {
    email: string;
    password: string;
}

export interface JwtPayload {
    userId: string;
}
