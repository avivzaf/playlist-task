import { songService } from "./song.services.js";
import { InstanceNotFoundError } from "../../exceptions/instanceNotFound.exception.js";
import { Request, Response } from "express";
import { ResponseMessage } from "../../types/response.types.js";

class SongController {
    createNewSong = async (req: Request, res: Response) => {
        const song = await songService.createNewSong(req.body);
        const reposonse: ResponseMessage = {
            message: "Song created succesfuly",
            status: 200,
            data: song,
        };

        res.status(200).json(reposonse);
    };

    getAllSong = async (req: Request, res: Response) => {
        const songs = await songService.getAllSongs();
        const reposonse: ResponseMessage = {
            message: "Songs fetched succesfuly",
            status: 200,
            data: songs,
        };

        res.status(200).json(reposonse);
    };

    getSingleSong = async (req: Request, res: Response) => {
        try {
            const song = await songService.getSingleSong(req.params.id);

            const reposonse: ResponseMessage = {
                message: "Song fetched succesfuly",
                status: 200,
                data: song,
            };

            res.status(200).json(reposonse);
        } catch (err) {
            throw new InstanceNotFoundError("Song not found", 404);
        }
    };

    updateSong = async (req: Request, res: Response) => {
        const song = await songService.updateSong(req.params.id, req.body);
        const reposonse: ResponseMessage = {
            message: "Song has been updated succesfuly",
            status: 200,
            data: song,
        };

        res.status(200).json(reposonse);
    };

    deleteSong = async (req: Request, res: Response) => {
        try {
            const song = await songService.deleteSong(req.params.id);
            const reposonse: ResponseMessage = {
                message: "Song has been deleted succesfuly",
                status: 200,
                data: song,
            };

            res.status(200).json(reposonse);
        } catch (err) {
            throw new InstanceNotFoundError("Song not found", 404);
        }
    };
}

export const songController = new SongController();
