import { songDB } from "./song.db.js";
import { IUpdateSong, ISong } from "./song.types.js";
import { artistService } from "../artist/artist.services.js";

class SongService {
    createNewSong = async (songDetails: ISong) => {
        const artistToUpdate = await artistService.getSingleArtist(
            songDetails.artist
        );
        const newSong = await songDB.createNewSong(songDetails);
        artistToUpdate.songs.push(newSong._id);
        await artistToUpdate.save();

        return newSong;
    };

    getAllSongs = async () => await songDB.getAllSongs();

    getSingleSong = async (id: string) => await songDB.getSingleSong(id);

    updateSong = async (songID: string, songDetails: IUpdateSong) => {
        if (songDetails.artist !== undefined) {
            artistService.removeSongFromArtist(songID);
            artistService.addSongToArtist(songID, songDetails.artist);
        }

        return await songDB.updateSong(songID, songDetails);
    };

    deleteSong = async (id: string) => {
        await artistService.removeSongFromArtist(id);
        //remove song form playlist

        return await songDB.deleteSong(id);
    };
}

export const songService = new SongService();
