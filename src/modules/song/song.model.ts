import mongoose from "mongoose";
const { Schema, model } = mongoose;

const SongSchema = new Schema({
    artist: { type: Schema.Types.ObjectId, ref: "artist" },
    name: {
        type: String,
        required: [true, "Song name is required"],
        minLength: 3,
        maxLength: 30,
    },
    lyrics: {
        type: String,
        required: [true, "Lyrics are required"],
        minLength: 30,
        maxLength: 300,
    },
    playlists: [{ type: Schema.Types.ObjectId, ref: "playlist" }],
});

export default model("song", SongSchema);
