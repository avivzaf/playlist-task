import raw from "../../utils/route.async.wrapper.js";
import express from "express";
import { validationFactoryMethod } from "./song.validation.js";
import { songController } from "./song.controllers.js";
import { logger } from "../../middleware/logger.handler.js";
import { SongValidation } from "./song.types.js"

const router = express.Router();



router.post(
    "/",
    raw(validationFactoryMethod(SongValidation.CREATE)),
    raw(songController.createNewSong)
);
router.get("/", raw(songController.getAllSong));
router.get("/:id", raw(songController.getSingleSong));
router.put(
    "/:id",
    raw(validationFactoryMethod(SongValidation.UPDATE)),
    raw(songController.updateSong)
);
router.delete("/:id", raw(songController.deleteSong));

export default router;
