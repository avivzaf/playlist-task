import Joi from "joi";
import { Request, Response, NextFunction } from "express";
import { ISong, IUpdateSong, SongValidation } from "./song.types.js";
import { InvalidArgumentsError } from "../../exceptions/invalidArguments.exception.js";

const validateSong = async (
    song: ISong | IUpdateSong,
    type: SongValidation = SongValidation.UPDATE
) => {
    let artist = Joi.string();
    let name = Joi.string().min(3).max(30);
    let lyrics = Joi.string().min(30).max(300);

    if (type == SongValidation.CREATE) {
        artist = artist.required();
        name = name.required();
        lyrics = lyrics.required();
    }

    const schema = Joi.object().keys({ artist, name, lyrics });

    try {
        return await schema.validateAsync(song);
    } catch (err: any) {
        throw new InvalidArgumentsError(err.details[0].message, 404);
    }
};

export const validationFactoryMethod = (type: SongValidation) => {
    return async (req: Request, res: Response, next: NextFunction) => {
        await validateSong(req.body, type);
        next();
    };
};
