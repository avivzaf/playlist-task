export interface ISong {
    artist: string;
    name: string;
    lyrics: string;
}

export type IUpdateSong = Partial<ISong>;

export enum SongValidation {
    CREATE,
    UPDATE,
}
