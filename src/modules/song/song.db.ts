import song_model from "./song.model.js";
import { IUpdateSong, ISong } from "./song.types.js";

class SongDB {
    getAllSongs = async () =>
        await song_model.find().select(`-_id 
        name 
        artist 
        jenar 
        lyrics
        playlists`);

    createNewSong = async (songDetails: ISong) =>
        await song_model.create(songDetails);

    getSingleSong = async (id: string) => await song_model.findById(id);

    updateSong = async (id: string, songDetails: IUpdateSong) =>
        await song_model.findByIdAndUpdate(id, songDetails, {
            new: true,
            upsert: false,
        });

    deleteSong = async (id: string) => await song_model.findByIdAndRemove(id);
}

export const songDB = new SongDB();
